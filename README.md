# OpenML dataset: avocado_sales

https://www.openml.org/d/43927

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Historical data on avocado prices and sales volume in multiple US markets. For this version Date column is dropped and month and day information in kept.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43927) of an [OpenML dataset](https://www.openml.org/d/43927). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43927/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43927/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43927/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

